# Passau - Wien
![Wien](images/wien.png)

## Tourendaten

* Distanz: 308km
* Höhenmeter: nein

## Wann

* 4-8 Mai
* Start Passau 4. Mai morgens (Mittwoch)
* Ankunft Wien 6. Mai abends (Freitags)
* Wochenende Wien ( Städtetrip ) 
* Sonntags heim

## Anfahrt

zb.: 06:03 Erlange - 09:37 Passau zu 23,90EUR

## Etappen

### Passau - Linz (98km)

Hotel: ?

### Linz - Pöchlarn/Melk (97km)

Hotel: ?

### Pöchlarn/Melk - Wien (113km)

Hotel: Ibis Budget Wien Messe. (Bjoern hat das schon aus ner Buchung von ner Firmenmesse von Maerz 2020 :) )
https://www.booking.com/hotel/at/ibis-budget-wien-messe.de.html
